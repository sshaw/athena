#include "TrigFake/FakeLvl1RoIatFixedEtaPhi.h"
#include "TrigFake/FakeLvl1MultipleRoIsatFixedEtaPhi.h"
//#include "TrigFake/FakeLvl1ConversionTestBeam.h"
#include "TrigFake/FakeLvl1RoIfromKine.h"
#include "TrigFake/ZVertexFromKine.h"


DECLARE_COMPONENT( FakeLvl1RoIfromKine )
  //DECLARE_COMPONENT( FakeLvl1ConversionTestBeam )
DECLARE_COMPONENT( FakeLvl1RoIatFixedEtaPhi )
DECLARE_COMPONENT( FakeLvl1MultipleRoIsatFixedEtaPhi )
DECLARE_COMPONENT( ZVertexFromKine )

