#include "CoolLumiUtilities/FillParamsTool.h"
#include "CoolLumiUtilities/BunchGroupTool.h"
#include "CoolLumiUtilities/BunchLumisTool.h"
#include "CoolLumiUtilities/OnlineLumiCalibrationTool.h"
#include "../OnlineLumiCalibrationCondAlg.h"

DECLARE_COMPONENT( FillParamsTool )
DECLARE_COMPONENT( BunchGroupTool )
DECLARE_COMPONENT( BunchLumisTool )
DECLARE_COMPONENT( OnlineLumiCalibrationTool )
DECLARE_COMPONENT( OnlineLumiCalibrationCondAlg )

